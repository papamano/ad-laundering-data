# Acknowledgments

This publication was funded in part by the European Union’s Internal Security Fund — Police under grant agreement 101038738 and in part under grant agreement 101132686.
The content of this publication represents the views of the author only and is his/her sole responsibility.
The European Commission does not accept any responsibility for use that may be made of the information it contains.
The work was also supported by the Foundation for Research and Technology Hellas through grant ESO00178.

## Resources

Project's icon created by [Freepik](https://www.flaticon.com/free-icons/fake-news) - [Flaticon](https://www.flaticon.com/).
