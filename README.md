# Ad Laundering: How Websites Deceive Advertisers into Rendering Ads Next to Illicit Content

## About

In this repository we make publicly available:
1. An aggregated list of anime, manga and piracy websites by merging open-source data.
2. A list of discovered websites that utilize Ad Laundering techniques to circumvent restrictions and monetize objectionable content.
3. A collection of screenshots showing how ad laundering websites can deceive legitimate and reputable companies into paying for ads next to low-quality content.

For more information please read our [paper](https://doi.org/10.1145/3589335.3651466), presented at the Web Conference 2024.
If you use any of these data sets in any way, please cite our work.

```
Emmanouil Papadogiannakis, Panagiotis Papadopoulos, Evangelos P. Markatos,
and Nicolas Kourtellis. 2024. Ad Laundering: How Websites Deceive Advertisers
into Rendering Ads Next to Illicit Content. In Companion Proceedings of the ACM
Web Conference 2024 (WWW ’24 Companion), May 13–17, 2024, Singapore, Singapore.
ACM, New York, NY, USA, 4 pages. https://doi.org/10.1145/3589335.3651466
```

## Contributors

* [Papadogiannakis Manos](https://gitlab.com/papamano/)

## Support

Please contact one of the project's contributors.

## License

This project is released under under the MIT License.
By installing, copying or otherwise using this dataset, you agree to be bound
by the terms of the provided License.
