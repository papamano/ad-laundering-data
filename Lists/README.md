# Sources

The following data sources were used in order to form the list of anime, manga and piracy websites.

| Source                                           | URL                                                                                  |
|--------------------------------------------------|--------------------------------------------------------------------------------------|
| SimilarWeb's top "Animation and Comics" websites | https://www.similarweb.com/top-websites/arts-and-entertainment/animation-and-comics/ |
| NextDNS' piracy blocklists                       | https://github.com/nextdns/piracy-blocklists                                         |
| Lightnovel Crawler                               | https://github.com/dipu-bd/lightnovel-crawler                                        |
| Manga OnlineViewer                               | https://github.com/TagoDR/MangaOnlineViewer                                          |
| Awesome Anime Sources                            | https://github.com/anshumanv/awesome-anime-sources                                   |